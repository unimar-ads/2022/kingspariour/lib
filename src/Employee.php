<?php
namespace gusta\lib;

use DateTime;


class Employee
{
    function __construct(
        public int $id,
        public string $name,
        public string $phone,
        public string $starting_time,
        public string $finishing_time
    )
    {
    }
}
$starting_time = "2012-05-21 22:02:00";
$finishing_time = "2020-05-21 22:02:00";

$d1 = new DateTime($starting_time);
$d2 = new DateTime($finishing_time);
$interval = $d2->diff($d1);

echo $interval->format('%d days, %H hours, %I minutes, %S seconds');