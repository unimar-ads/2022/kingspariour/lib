<?php

namespace gusta\lib;

class Activity
{
    function __construct(
        public string $name,
        public string $description,
        public string $duration,
        public float $value
    )
    {
    }
}