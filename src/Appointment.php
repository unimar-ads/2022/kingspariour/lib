<?php
namespace gusta\lib;
use DateTime;

class Appointment{   /*
    function __construct( 
    public int $dia,
    public DateTime $hora ,
    public int $procedimento,
    public int $profissional,
    public float $valor){
   */
}



/**
* Classe abstrata MeuCalendario
* 
* @abstract class
*/
abstract class MeuCalendario
{
 private $ano;
 private $mes;
 private $diasDoMes;
 private $diasDoMesAnterior;
 private $diasDoMesSeguinte;
 private $siglasDiasSemana = array("Dom", "Seg", "Ter", "Qua", "Qui", "Sex", "Sáb");
 private $nomesMesesAno = array("Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro");

 public function __construct($mes, $ano)
 {
     if (is_int($mes) && $mes <= 12 || $mes >= 1) {
         $this->mes = $mes;
     } else {
         throw new Exception("Erro: o valor definido para mês não é válido: ".$mes);
     }

     if (is_int($ano) && $ano >= 1970 || $ano <= 2099) {
         $this->ano = $ano;
     } else {
         throw new Exception("Erro: o valor definido para ano não é válido: ".$ano);
     }

     // Primeiro verificar se existem dias do mês anterior que aparecem na folha do calendário
     if ($this->isDiasMesAnterior()) {
         $this->diasDoMesAnterior = $this->setDiasDoMesAnterior();
     }

     // Também verificar se existem dias do mês seguinte que aparecem na folha do calendário
     if ($this->isDiasMesSeguinte()) {
         $this->diasDoMesSeguinte = $this->setDiasMesSeguinte();
     }

     // Chamar método que monta os dias na folha do calendário
     $this->diasDoMes = $this->setDiasCalendario();
 }

/**
 * Verifica se há dias do mês anterior no calendário
 * @return boolean
 */
 protected function isDiasMesAnterior()
 {
     return date('w', mktime(0, 0, 0, $this->mes, 1, $this->ano)) > 0 ? TRUE : FALSE;
 }

/**
 * Verifica se há dias do mês seguinte no calendário
 * @return boolean
 */
 protected function isDiasMesSeguinte()
 {
     return date('w', strtotime("last day of this month", mktime(0, 0, 0, $this->mes, 1, $this->ano))) <= 6 ? TRUE : FALSE;
 }

/**
 * Retorna um array contendo os dias do mês anterior que aparecem na folha do calendário
 * @return array
 */
 private function setDiasDoMesAnterior()
 {
     $diaNaSemana = date('w', strtotime("{$this->ano}-{$this->mes}-01 - 1 day"));
     $dia = date('d', strtotime("{$this->ano}-{$this->mes}-01 - 1 day")) - $diaNaSemana;

     // Preenche array
     for ($diaSemana = 0; $diaSemana <= $diaNaSemana; $diaSemana++) {
         $arrayDias[] = (int) $dia++;
     }

     return $arrayDias;
 }

/**
 * Retorna um array contendo os dias do mês seguinte que aparecem na folha do calendário
 * @return array
 */
 private function setDiasMesSeguinte()
 {
     $diaNaSemana = date('w', strtotime("+1 month", mktime(0, 0, 0, $this->mes, 1, $this->ano)));
     $dia = 1;

     // Preenche array
     for ($diaSemana = $diaNaSemana; $diaSemana <= 6; $diaSemana++) {
         $arrayDias[] = (int) $dia++;
     }

     // Adicionar mais uma semana para evitar perder a quinta semana de alguns meses
     for ($add = 0; $add <= 6; $add++) {
        $arrayDias[] = (int) $dia++;
     }

     // Adicionar mais uma semana para evitar perder a quinta semana de alguns meses
     for ($add = 0; $add <= 6; $add++) {
         $arrayDias[] = (int) $dia++;
     }

     return $arrayDias;
 }

 private function setDiasCalendario()
 {
     // Descobre qual o ultimo dia do mês informado
     $ultimoDiaMes = date('t', mktime(0, 0, 0, $this->mes, 1, $this->ano));

     // Preenche array
     for ($dia = 1; $dia <= $ultimoDiaMes; $dia++) {
         $arrayDias[] = (int) $dia;
     }

     // Adicionar os dias do mês anterior, se houver
     if ($this->isDiasMesAnterior()) {
         $arrayDias = array_merge($this->getDiasDoMesAnterior(), $arrayDias);
     }

     // Adicionar os dias do mês seguinte, se houver
     if ($this->isDiasMesSeguinte()) {
         $arrayDias = array_merge($arrayDias, $this->getDiasDoMesSeguinte());
     }

     // Evitar falhas para meses que terminam no dia de sábado com dia 30
     if(count($arrayDias) < 41){
         for($dia = 1; $dia <= 7; $dia++){
             $arrayDias[] = (int) $dia;
         }
     }

     return $arrayDias;
 }

/**
 * Retorna um array com os dias do mês anterior que ainda aparecem na folha do calendário
 * @return array
 */
 private function getDiasDoMesAnterior()
 {
     return $this->diasDoMesAnterior;
 }

/**
 * Retorna um array com os dias do mês seguinte que já aparecem na folha do calendário
 * @return array
 */
 private function getDiasDoMesSeguinte()
 {
     return $this->diasDoMesSeguinte;
 }

/**
 * Retorna a sigla da semana conforme um número de indide informado
 * @param int $diaSemana
 * @return String "Sigla do dia da semana. Ex: Seg, Ter, Qua..."
 * @throws Exception
 */
 protected function getSiglaPorDia($diaSemana)
 {
     if (is_int($diaSemana) && $diaSemana <= 6 || $diaSemana >= 0) {
         return $this->siglasDiasSemana[$diaSemana];
     } else {
         throw new Exception("Erro: Valor numérico invalido para se obter nome do dia da semana");
     }
 }

/**
 * Retorna o nome do Mês corrente por extenso
 * @return string
 */
 protected function getNomeMes()
 {
     return $this->nomesMesesAno[$this->mes - 1];
 }

/**
 * Retorna um array comos dias definidos para folha do calendário
 * @return array
 */
 protected function getDiasDoMes()
 {
     return $this->diasDoMes;
 }

/**
 * Retorna o mês no formato numérico
 * @return int
 */
 protected function getMes()
 {
     return $this->mes;
 }

/**
 * Retorna o ano no formato numérico
 * @return int
 */

 protected function getAno()
 {
     return $this->ano;
 }

 public abstract function getCalendario();
}
