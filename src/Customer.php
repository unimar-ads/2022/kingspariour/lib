<?php

namespace gusta\lib;

class Customer{
    function __construct(
        public string $name,
        public string $cpf, 
        public string $phoneString,
        public string $email,
        public string $user,
        public string $password
        #Pessoa::brazilianPhoneParser($phoneString, $forceOnlyNumber)
        )
    {
    }
    function validaCPF()
    {
        $cpf = $this->CPF;
        // Extrai somente os números
        $cpf = preg_replace('/[^0-9]/is', '', $cpf);

        // Verifica se foi informado todos os digitos corretamente
        if (strlen($cpf) != 11) {
            return false;
        }

        // Verifica se foi informada uma sequência de digitos repetidos. Ex: 111.111.111-11
        if (preg_match('/(\d)\1{10}/', $cpf)) {
            return false;
        }

        // Faz o calculo para validar o CPF
        for ($t = 9; $t < 11; $t++) {
            for ($d = 0, $c = 0; $c < $t; $c++) {
                $d += $cpf[$c] * (($t + 1) - $c);
            }
            $d = ((10 * $d) % 11) % 10;
            if ($cpf[$c] != $d) {
                return false;
            }
        }
        return true;

    }
    static function brazilianPhoneParser(string $phoneString, bool $forceOnlyNumber = true) : ?array
    {
        $phoneString = preg_replace('/[()]/', '', $phoneString);
        if (preg_match('/^(?:(?:\+|00)?(55)\s?)?(?:\(?([0-0]?[0-9]{1}[0-9]{1})\)?\s?)??(?:((?:9\d|[2-9])\d{3}\-?\d{4}))$/', $phoneString, $matches) === false) {
            return null;
        }
    
        $ddi = $matches[1] ?? '';
        $ddd = preg_replace('/^0/', '', $matches[2] ?? '');
        $number = $matches[3] ?? '';
        if ($forceOnlyNumber === true) {
            $number = preg_replace('/-/', '', $number);
        }
    
        return ['ddi' => $ddi, 'ddd' => $ddd , 'number' => $number];
    }
}

$customer = new Customer('nome', '01234567890', '4002-8922', 'email@email.com', 'user', 'password');
var_dump($customer);